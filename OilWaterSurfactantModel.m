classdef OilWaterSurfactantModel < TwoPhaseOilWaterModel
    % Oil/water/surfactant system
    % This model is a two phase oil/water model, extended with the
    % surfactant phase in addition.
    % This is essentially OilWaterPolymerModel.m
    
    properties
        % Surfactant present
        surfactant
        
    end
    
    methods
        function model = OilWaterSurfactantModel(G, rock, fluid, varargin)
            
            model = model@TwoPhaseOilWaterModel(G, rock, fluid);
            
            % This is the model parameters for oil/water/surfactant
            model.surfactant = true;
            
            model.wellVarNames = {'qWs', 'qOs', 'qWSurf', 'bhp'};
            
            model = merge_options(model, varargin{:});
            
        end
        
        function [problem, state] = getEquations(model, state0, state, ...
                dt, drivingForces, varargin)
            [problem, state] = equationsOilWaterSurfactant(state0, state, ...
                model, dt, drivingForces, varargin{:});
        end
                      
        function [fn, index] = getVariableField(model, name)
            % Get the index/name mapping for the model (such as where
            % pressure or water saturation is located in state)
            switch(lower(name))
                case {'surfactant'}
                    index = 1;
                    fn = 'c';
                otherwise
                    [fn, index] = getVariableField@TwoPhaseOilWaterModel(...
                                    model, name);
            end
        end
    end
end
