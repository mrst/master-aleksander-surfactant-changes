clear all
close all
mrstModule add deckformat ad-fi
current_dir = fileparts(mfilename('fullpath'));
fn = fullfile(current_dir, 'input.data');

deck = readEclipseDeck(fn);
deck = convertDeckUnits(deck);

G = initEclipseGrid(deck);
G = computeGeometry(G);
rock = initEclipseRock(deck);
rock = compressRock(rock, G.cells.indexMap);
fluid = initDeckADIFluid(deck);
gravity reset on

state0 = initResSol(G, 300*barsa, [.2,.8]);
state0.c    = zeros(G.cells.num, 1);
modelSurfact = OilWaterSurfactantModel(G, rock, fluid, 'inputdata', deck);
modelOW = TwoPhaseOilWaterModel(G, rock, fluid, 'inputdata', deck);
schedule = convertDeckScheduleToMRST(G, modelSurfact, rock, deck);
%% Both solutions with and without surfactant
[wellSolsSurfact, statesSurfact] = ...
   simulateScheduleAD(state0, modelSurfact, schedule);
[wellSolsOW, statesOW] = simulateScheduleAD(state0, modelOW, schedule);
