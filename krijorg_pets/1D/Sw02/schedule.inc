NOECHO

-- Schedule include file

WCONPROD
--      WELL     OPEN/  CNTL   OIL  WATER   GAS  LIQU   RES   BHP
--      NAME     SHUT   MODE  RATE   RATE  RATE  RATE  RATE
     'PROD' 'OPEN'  'BHP'  5*                            280/
/


WCONINJE
--      WELL        INJ     OPEN/   CNTL    FLOW    RES     BHP
--      NAME        TYPE    SHUT    MODE    RATE    RATE    
     'INJ'    'WATER' 'OPEN'  'BHP'   2*              320/
/

WSURFACT
  'INJ'   0.0 /
/

TSTEP
10*1 10*3 175*4
/


WCONPROD
--      WELL     OPEN/  CNTL   OIL  WATER   GAS  LIQU   RES   BHP
--      NAME     SHUT   MODE  RATE   RATE  RATE  RATE  RATE
     'PROD' 'OPEN'  'BHP'  5*                            280/
/


WCONINJE
--      WELL        INJ     OPEN/   CNTL    FLOW    RES     BHP
--      NAME        TYPE    SHUT    MODE    RATE    RATE    
     'INJ'    'WATER' 'OPEN'  'BHP'   2*              320/
/

WSURFACT
  'INJ'   50.0 /
/

TSTEP
10*1 10*3 175*4
/

WCONPROD
--      WELL     OPEN/  CNTL   OIL  WATER   GAS  LIQU   RES   BHP
--      NAME     SHUT   MODE  RATE   RATE  RATE  RATE  RATE
     'PROD' 'OPEN'  'BHP'  5*                            280/
/


WCONINJE
--      WELL        INJ     OPEN/   CNTL    FLOW    RES     BHP
--      NAME        TYPE    SHUT    MODE    RATE    RATE    
     'INJ'    'WATER' 'OPEN'  'BHP'   2*              320/
/

WSURFACT
  'INJ'   0.0 /
/

TSTEP
10*1 10*3 175*4
/


ECHO